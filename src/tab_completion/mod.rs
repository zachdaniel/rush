use std::env;
use std::fs;
use std::cmp::Ordering;


pub fn complete(search: &str) -> Vec<String> {
    let mut matches = vec![];
    matches.extend(complete_builtin(search));
    matches.extend(complete_executable(search));
    let string_search = String::from(search);
    matches.sort_by(|elem1, elem2| {
        if elem1 == &string_search {
            return Ordering::Less;
        } else if elem2 == &string_search {
            return Ordering::Greater;
        } else {
            return Ordering::Equal;
        }
    });
    matches.dedup();
    return matches;
}

pub fn complete_builtin(search: &str) -> Vec<String> {
    let builtins: Vec<&str> = vec!["exit", "cd", "export"];
    return builtins.into_iter()
        .filter(|candidate| candidate.starts_with(search))
        .map(|matched| matched.to_string())
        .collect();
}

pub fn complete_executable(search: &str) -> Vec<String> {
    let mut matches: Vec<String> = vec![];
    if let Some(path) = env::var_os("PATH") {
        for dirname in env::split_paths(&path) {
            if let Ok(dir) = fs::read_dir(dirname) {
                for potential_entry in dir {
                    if let Ok(entry) = potential_entry {
                        if let Some(name) = entry.file_name().to_str() {
                            if name.starts_with(search) {
                                matches.push(name.to_string());
                            }
                        }
                    }
                }
            }
        }
    }
    return matches;
}
