#[macro_use]
extern crate nom;
#[macro_use]
extern crate lazy_static;
extern crate linenoise;
extern crate libc;

use libc::funcs::posix01::signal::signal;
use libc::SIGINT;
use std::collections::HashMap;
use std::sync::Mutex;

mod parse;
mod tab_completion;
use parse::{rushrc, prompt};

lazy_static! {
    static ref BINDINGS: Mutex<HashMap<String, String>> = Mutex::new(HashMap::new());
}
lazy_static! {
    static ref ALIAS: Mutex<HashMap<String, String>> = Mutex::new(HashMap::new());
}

fn main() {
    unsafe { signal(SIGINT, interrupt as u64) };
    rushrc::init();
    linenoise::set_callback(tab_completion::complete);
    loop {
        let val = linenoise::input(&prompt::prompt());
        match val {
            Some(input) => {
                if input.is_empty() {
                    // do nothing
                } else {
                    match parse::parse(input.clone()).unwrap() {
                        None => {
                            // do nothing
                        }
                        Some(com) => {
                            let res = com.spawn();
                            if let Err(ref err) = res {
                                println!("{}", err);
                            }
                            linenoise::history_add(&input);
                        }
                    }
                }
            }
            None => {}
        };
    }
}

extern "C" fn interrupt(sig: u32) {
    ()
}
