use ALIAS;

pub fn alias(input: String) -> String {
    let mut workspace = input;
    for (name, value) in ALIAS.lock().unwrap().iter() {
        workspace = workspace.replace(name, value)
    }
    workspace
}
