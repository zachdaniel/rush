mod token;
mod linked_command;
mod aliaser;
pub mod rushrc;
pub mod prompt;

use nom::{is_alphanumeric, GetInput, space, IResult, Needed, newline, multispace};
use nom::Err as NomErr;
use nom::ErrorKind as NomErrKind;

use self::token::Token;
use self::linked_command::LinkedCommand;
use self::aliaser::alias;

use std::fmt;
use std::path::PathBuf;
use std::fs::File;
use std::io;
use std::io::Read;
use std::env;

const OUTPUT_LIMIT: usize = 40;

use BINDINGS;

named!(herestring <Token>,
    map!(
        delimited!(
            opt!(space),
            chain!(
                tag!("<<") ~
                delim: take_while!(is_alphanumeric) ~
                space ~
                here: take_until_and_consume!(delim),
                || { return here }
            ),
            opt!(space)
        ),
        Token::herestring
    )
);

named!(quoted <Token>,
    map!(
        alt!(
            delimited!(tag!("\""), is_not!("\""), tag!("\"")) |
            delimited!(tag!("'"), is_not!("'"), tag!("'"))
        ),
        Token::quoted
    )
);

named!(bound <Token>,
    chain!(
        name: preceded!(tag!("$"), word),
        || {
            match BINDINGS.lock().unwrap().get(&name.value) {
                Some(val) => Token::word(val.as_bytes()),
                None => {
                    match env::var(name.clone().value) {
                        Ok(val) => Token::word(val.as_bytes()),
                        Err(_) => {
                            Token::word("".as_bytes())
                       }
                    }
                }
            }
        }
    )
);

named!(tilde <Token>,
    chain!(
        tag!("~") ~
        rest: useable,
        || {
            Token::word(format!("{}{}", env::var("HOME").unwrap_or(String::from("")), rest.value).as_bytes())
        }
    )
);

named!(pipe <Token>,
    map!(
        delimited!(opt!(space), tag!("|"), opt!(space)),
        Token::pipe
    )
);

named!(left_caret <Token>,
    map!(
        delimited!(opt!(space), tag!("<"), opt!(space)),
        Token::left_caret
    )
);

named!(right_caret <Token>,
    map!(
        delimited!(opt!(space), tag!(">"), opt!(space)),
        Token::right_caret
    )
);

named!(useable <Token>,
    delimited!(
        opt!(space),
        alt!(
            bound |
            herestring |
            quoted |
            tilde |
            word
        ),
        opt!(space)
    )
);

named!(useable_in_binding <Token>,
    alt!(
        herestring |
        quoted |
        word_binding
    )
);

named!(and_sym <Token>,
    map!(
        delimited!(opt!(space), tag!("&&"), opt!(space)),
        Token::and
    )
);

named!(single_command <LinkedCommand>,
    chain!(
        bindings: many0!(binding) ~
        name: useable ~
        args: many0!(useable) ~
        opt!(newline),
        || { let mut command = LinkedCommand::new(name.value.clone());
            for token in args {
                command.arg(token.value);
            }
            for (var, val) in bindings {
                BINDINGS.lock().unwrap().insert(var.value, val.value);
            }
            command
        }
    )
);

named!(in_file <PathBuf>,
    chain!(
        not_eof ~
        left_caret ~
        file_name: useable,
        || { PathBuf::from(file_name.value.clone()) }
    )
);

named!(out_file <PathBuf>,
    chain!(
        not_eof ~
        right_caret ~
        file_name: useable,
        || { PathBuf::from(file_name.value.clone())}
    )
);

named!(piped_into <LinkedCommand>,
    chain!(
        not_eof ~
        pipe ~
        command: command,
        || {return command}
    )
);

named!(and_then <LinkedCommand>,
    chain!(
        not_eof ~
        and_sym ~
        command: command,
        || {return command}
    )
);

named!(command <LinkedCommand>,
    chain!(
        mut comm: single_command ~
        in_file_path: opt!(in_file) ~
        out_file_path: opt!(out_file) ~
        piped: opt!(piped_into) ~
        and_then: opt!(and_then),
        || {
            comm.piped_to = piped.map(|c| Box::new(c));
            comm.file_in = in_file_path;
            comm.file_out = out_file_path;
            comm.and = and_then.map(|c| Box::new(c));
            comm
        }
    )
);

named!(binding <(Token, Token)>,
    chain!(
        opt!(multispace) ~
        var: useable_in_binding ~
        tag!("=") ~
        val: useable_in_binding ~
        opt!(multispace),
        || {
            (var, val)
        }
    )
);

pub fn word(input: &[u8]) -> IResult<&[u8], Token> {
    let mut i = 0;
    let mut word: Vec<u8> = vec![];
    while i < input.len() {
        if is_not_non_word(input[i]) {
            word.push(input[i]);
            i += 1;
        } else if input[i] == '\\' as u8 {
            i += 1;
            if i < input.len() {
                word.push(input[i]);
                i += 1;
            } else {
                return IResult::Incomplete(Needed::Unknown);
            }
        } else {
            break;
        }
    }
    if word.is_empty() {
        return IResult::Error(NomErr::Code(NomErrKind::Custom(101)));
    } else {
        let token = Token::word(&word);
        IResult::Done(&input[i..], token)
    }
}

fn is_not_non_word(chr: u8) -> bool {
    chr != ' ' as u8 && chr != '\t' as u8 && chr != '\r' as u8 && chr != '\n' as u8 &&
    chr != '\'' as u8 && chr != '"' as u8 && chr != '|' as u8 &&
    chr != '>' as u8 && chr != '<' as u8 && chr != '&' as u8 && chr != '\\' as u8
}

pub fn word_binding(input: &[u8]) -> IResult<&[u8], Token> {
    let mut i = 0;
    let mut word: Vec<u8> = vec![];
    while i < input.len() {
        if non_word_no_eql(input[i]) {
            word.push(input[i]);
            i += 1;
        } else if input[i] == '\\' as u8 {
            i += 1;
            if i < input.len() {
                word.push(input[i]);
                i += 1;
            } else {
                return IResult::Incomplete(Needed::Unknown);
            }
        } else {
            break;
        }
    }
    if word.is_empty() {
        return IResult::Error(NomErr::Code(NomErrKind::Custom(101)));
    } else {
        let token = Token::word(&word);
        IResult::Done(&input[i..], token)
    }
}

fn non_word_no_eql(chr: u8) -> bool {
    is_not_non_word(chr) && chr != '=' as u8
}

pub fn not_eof(input: &[u8]) -> IResult<&[u8], &[u8]> {
    if input.len() == 0 {
        IResult::Error(NomErr::Code(NomErrKind::Custom(99)))
    } else {
        IResult::Done(input, input)
    }
}

// Had to use many0 instead of many1 due to issues with lifetimes
// TODO: Fix this, once the library has fixed it.
// This will leak into the parse method as well.  Will need a fair bit of refactoring.
named!(commandeer(&[u8]) -> (Option<LinkedCommand>),
    chain!(
        bindings: many0!(binding) ~
        command: opt!(delimited!(opt!(multispace), command, opt!(multispace))),
        || {
            for (var, val) in bindings {
                BINDINGS.lock().unwrap().insert(var.value, val.value);
            }
            command
        }
    )
);

pub fn parse(line: String) -> Result<Option<LinkedCommand>, LexError> {
    let aliased = alias(line);
    let result = commandeer(aliased.as_bytes());
    if result.remaining_input().map_or(false, |c| !c.is_empty()) {
        Err(LexError::UnclosedDelimiter(gist(result.remaining_input().unwrap())))
    } else {
        match result {
            IResult::Done(_, output) => Ok(output),
            IResult::Error(error) => Err(LexError::NomError(NomErr::Code(NomErrKind::Custom(1)))),
            IResult::Incomplete(_) => Err(LexError::UnknownError),
        }
    }
}

pub fn parse_file(filename: &str) -> Result<Option<LinkedCommand>, LexError> {
    let mut contents = String::new();
    let mut file = try!(File::open(filename));
    try!(file.read_to_string(&mut contents));
    let aliased = alias(contents);
    let result = commandeer(aliased.as_bytes());
    if result.remaining_input().map_or(false, |c| !c.is_empty()) {
        Err(LexError::UnclosedDelimiter(gist(result.remaining_input().unwrap())))
    } else {
        match result {
            IResult::Done(_, output) => Ok(output),
            IResult::Error(_) => Err(LexError::UnknownError), //TODO: Fixme
            IResult::Incomplete(_) => Err(LexError::UnknownError),
        }
    }
}

#[derive(Debug)]
pub enum LexError {
    UnclosedDelimiter(String),
    NomError(NomErr<u8>),
    IoError(io::Error),
    UnknownError,
}

impl From<io::Error> for LexError {
    fn from(err: io::Error) -> Self {
        LexError::IoError(err)
    }
}

impl fmt::Display for LexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            LexError::IoError(ref err) => write!(f, "{}", err),
            LexError::NomError(ref err) => write!(f, "Parse error: {:?}", err),
            LexError::UnclosedDelimiter(ref err) => write!(f, "Unclosed delimiter at - {}", err),
            LexError::UnknownError => write!(f, "An unknown error occured."),
        }
    }
}

fn gist(remaining: &[u8]) -> String {
    String::from_utf8(limit(remaining).to_vec()).unwrap()
}

fn limit(remaining: &[u8]) -> &[u8] {
    if remaining.len() <= OUTPUT_LIMIT {
        remaining
    } else {
        &remaining[0..OUTPUT_LIMIT]
    }
}
