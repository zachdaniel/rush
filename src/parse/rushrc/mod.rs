use std::env;

use parse;

pub fn init() {
    let home = format!("{}{}", env::var("HOME").unwrap(), "/.rushrc");
    let linked_command = parse::parse_file(&home);
    let _ = linked_command.map(|x| x.map(|x| x.spawn()));
}
