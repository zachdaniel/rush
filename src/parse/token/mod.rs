use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    HereString,
    Quoted,
    Pipe,
    LeftCaret,
    RightCaret,
    Word,
    And,
}

#[derive(Clone, PartialEq)]
pub struct Token {
    pub value: String,
    pub token_type: TokenType,
}

impl Token {
    pub fn word(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::Word,
        }
    }

    pub fn pipe(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::Pipe,
        }
    }

    pub fn left_caret(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::LeftCaret,
        }
    }

    pub fn right_caret(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::RightCaret,
        }
    }

    pub fn herestring(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::HereString,
        }
    }

    pub fn quoted(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::Quoted,
        }
    }

    pub fn and(value: &[u8]) -> Token {
        Token {
            value: String::from_utf8(value.to_vec()).unwrap(),
            token_type: TokenType::And,
        }
    }
}

impl fmt::Display for Token {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{:?}: {}", self.token_type, self.value)
    }
}

impl fmt::Debug for Token {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{:?}: '{}'", self.token_type, self.value)
    }
}
