use std::env;
use std::process::Command;

use nom::IResult;

extern crate time;

named!(username <String>,
	chain!(
		tag!("\\u"),
		|| {
			return user()
		}
	)
);

named!(host <String>,
	chain!(
		tag!("\\h"),
		|| {
			return hostname()
		}
	)
);

named!(dir <String>,
	chain!(
		tag!("\\d"),
		|| {
			return current_dir()
		}
	)
);

named!(carriage_return <String>,
	chain!(
		tag!("\\r"),
		|| {
			return String::from("\n")
		}
	)
);

named!(time_24 <String>,
	chain!(
		tag!("\\t") ~
		fmt: delimited!(tag!("["), is_not!("]"), tag!("]")),
		|| {
			return time::strftime(&String::from_utf8_lossy(fmt), &time::now()).unwrap_or(String::from("<time error>"))
		}
	)
);

named!(opt <String>,
	alt!(
		username |
		host |
		dir |
		time_24 |
		carriage_return |
		chain!(
			chr: take!(1),
			|| { String::from_utf8(chr.to_vec()).unwrap()  }
		)
	)
);

named!(promptify <String>,
	chain!(
		opts: many0!(opt),
		|| {
			opts.join("")
		}
	)
);


pub fn prompt() -> String {
    let to_parse = env::var("PROMPT");
    match to_parse {
        Ok(var) => {
            match promptify(var.as_bytes()) {
                IResult::Done(_, output) => output,
                IResult::Incomplete(_) => String::from("Prompt Parse Error"),
                IResult::Error(err) => format!("{:?}", err),
            }
        }
        Err(e) => format!("{}", e),
    }
}

fn user() -> String {
    let output = Command::new("id").arg("-un").output();
    match output {
        Ok(output) => {
            let stdout = String::from_utf8_lossy(&output.stdout);
            let stderr = String::from_utf8_lossy(&output.stderr);
            format!("{}{}", stdout.trim(), stderr.trim())
        }
        Err(_) => "".to_string(),
    }
}

fn hostname() -> String {
    let output = Command::new("hostname").arg("-s").output();
    match output {
        Ok(output) => {
            let stdout = String::from_utf8_lossy(&output.stdout);
            let stderr = String::from_utf8_lossy(&output.stderr);
            format!("{}{}", stdout.trim(), stderr.trim())
        }
        Err(_) => "".to_string(),
    }
}

fn current_dir() -> String {
    match env::current_dir() {
        Ok(pathbuf) => pathbuf.to_str().unwrap_or("<unknown>").to_string(),
        Err(e) => format!("{:?}", e),
    }
}
